import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

import User from 'App/Models/Manager/User';

export default class UsersController {

  public async index({ response }: HttpContextContract) {
    let data = await User.query();
    return response.json(data);
  }

  public async store({ request, response }: HttpContextContract) {

    let data = request.post();

    const emailExists = await User.query().where('email', data.email);
    
    if (!emailExists) {
      const query = await User.create(data);

      if (query) {
        return response.json({
          result: true,
          message: 'Usuário criado com sucesso',
          data: query
        })
      }
      else {
        return response.status(400).json({
          result: false,
          message: 'Ocorreu um problema ao criar o usuario',
          data: query
        })
      }
    }
    else {
      return response.status(400).json({
        result: false,
        message: 'Ja existe um usuario com o email informado',
      })
    }
  }

  public async show({ }: HttpContextContract) {
  }

  public async update({ }: HttpContextContract) {
  }

  public async destroy({ }: HttpContextContract) {
  }
}
