import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Event from '@ioc:Adonis/Core/Event'


import User from 'App/Models/Manager/User';
import Survey from 'App/Models/Manager/Survey';
import SurveyUser from 'App/Models/Manager/SurveyUser';

export default class SendMailsController {
  public async execute({ request, response }: HttpContextContract) {
    const { email, survey_id } = request.post();
    const user = await User.findBy('email', email);
    if (user) {
      const survey = await Survey.findBy('id', survey_id);

      if (survey) {
        const surveyUser = {
          user_id: user.id,
          survey_id: survey_id
        }

        const querySurveyUser = await SurveyUser.create(surveyUser);

        const emailBody = {
          name: user.name,
          email: user.email,
          title: survey.title,
          description: survey.description
        };
        
        Event.emit('new:reply-survey', emailBody);

        return response.json({
          result: true,
          data: querySurveyUser
        });

      }
      else {
        return response.status(400).json({
          result: false,
          message: 'Nao foi possivel encontrar a pesquisa informada'
        })
      }
    }
    else {
      return response.status(400).json({
        result: false,
        message: 'Nao foi possivel encontrar o usuario informado'
      })
    }
  }
}
