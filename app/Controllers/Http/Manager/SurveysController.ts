import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Survey from 'App/Models/Manager/Survey';


export default class SurveysController {
  public async index({ response }: HttpContextContract) {
    const data = await Survey.all();

    return response.json({
      data: data
    });
    
  }

  public async store({ request, response }: HttpContextContract) {
    const data = request.post();
    const query = await Survey.create(data);

    return response.json({
      data: query
    });
  }

  public async show({ }: HttpContextContract) {
  }

  public async update({ }: HttpContextContract) {
  }

  public async destroy({ }: HttpContextContract) {
  }
}
