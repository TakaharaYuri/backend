import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class SurveyUsers extends BaseSchema {
  protected tableName = 'survey_users'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.timestamps(true)
      table.string('name')
      table.integer('user_id')
          .unsigned()
          .notNullable()
          .references('id')
          .inTable('users')
          .onUpdate('CASCADE')
          .onDelete('CASCADE')
      table.integer('survey_id')
          .unsigned()
          .notNullable()
          .references('id')
          .inTable('surveys')
          .onUpdate('CASCADE')
          .onDelete('CASCADE')
      table.integer('value')
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
