import Event from '@ioc:Adonis/Core/Event'
import Mail from '@ioc:Adonis/Addons/Mail'
import Logger from '@ioc:Adonis/Core/Logger'



Event.on('new:reply-survey', (data) => {
  console.log(data);
  Logger.info('Iniciando envio de e-mail [reply-survey]');

  Mail.send((message) => {
    message
      .from('nwl@fdvweb.com.br')
      .to(data.email)
      .subject('Welcome Onboard!')
      .htmlView('emails/reply_survey', { data: data })
  });

  Logger.info('Envio de E-mail finalizado');
})